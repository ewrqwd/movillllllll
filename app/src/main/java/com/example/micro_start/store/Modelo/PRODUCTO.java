package com.example.micro_start.store.Modelo;

public class PRODUCTO {
    private int Id;
    private int Id_Categoria;
    private int Id_Vendedor;
    private String Codigo;
    private String Nombre;
    private String Descripcion;
    private Double Precio;
    private Boolean Estado;

    public PRODUCTO() {
    }

    public PRODUCTO(int id, int id_Categoria, int id_Vendedor, String codigo, String nombre, String descripcion, Double precio, Boolean estado) {
        Id = id;
        Id_Categoria = id_Categoria;
        Id_Vendedor = id_Vendedor;
        Codigo = codigo;
        Nombre = nombre;
        Descripcion = descripcion;
        Precio = precio;
        Estado = estado;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getId_Categoria() {
        return Id_Categoria;
    }

    public void setId_Categoria(int id_Categoria) {
        Id_Categoria = id_Categoria;
    }

    public int getId_Vendedor() {
        return Id_Vendedor;
    }

    public void setId_Vendedor(int id_Vendedor) {
        Id_Vendedor = id_Vendedor;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public Double getPrecio() {
        return Precio;
    }

    public void setPrecio(Double precio) {
        Precio = precio;
    }

    public Boolean getEstado() {
        return Estado;
    }

    public void setEstado(Boolean estado) {
        Estado = estado;
    }
}
