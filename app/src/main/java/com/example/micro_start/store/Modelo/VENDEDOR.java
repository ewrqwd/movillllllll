package com.example.micro_start.store.Modelo;

public class VENDEDOR {
    private   int ID ;
    private   int Id_Rol ;
    private   String Nombre ;
    private   String Apellido ;
    private   String Correo ;
    private   String Clave  ;
    private    Boolean  Estado  ;

    public VENDEDOR() {
    }

    public VENDEDOR(int ID, int id_Rol, String nombre, String apellido, String correo, String clave,  Boolean  estado) {
        this.ID = ID;
        Id_Rol = id_Rol;
        Nombre = nombre;
        Apellido = apellido;
        Correo = correo;
        Clave = clave;
        Estado = estado;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getId_Rol() {
        return Id_Rol;
    }

    public void setId_Rol(int id_Rol) {
        Id_Rol = id_Rol;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public String getClave() {
        return Clave;
    }

    public void setClave(String clave) {
        Clave = clave;
    }

    public Boolean getEstado() {
        return Estado;
    }

    public void setEstado( Boolean  estado) {
        Estado = estado;
    }
}
