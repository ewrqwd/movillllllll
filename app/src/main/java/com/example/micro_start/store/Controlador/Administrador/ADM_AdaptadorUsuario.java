package com.example.micro_start.store.Controlador.Administrador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.micro_start.store.Modelo.VENDEDOR;
import com.example.micro_start.store.R;

import java.util.ArrayList;

public class ADM_AdaptadorUsuario extends BaseAdapter {
    private static Context context;
    private static ArrayList<VENDEDOR> ListaItem;
    private static ListView Lista_View;

    public ADM_AdaptadorUsuario(Context context, ArrayList<VENDEDOR> listaItem, ListView lista_view) {
        this.context = context;
        this.ListaItem = listaItem;
        this.Lista_View = lista_view;
    }


    @Override
    public int getCount() {//cantidad de contenido
        return ListaItem.size();
    }

    @Override
    public Object getItem(int position) {//devuelve el list item de la posicion
        return ListaItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VENDEDOR Item = (VENDEDOR) getItem(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.item_usuario, null);
        TextView Nombre = (TextView) convertView.findViewById(R.id.ItemNombre);
        TextView Apellido = (TextView) convertView.findViewById(R.id.ItemApellido);
        TextView Correo = (TextView) convertView.findViewById(R.id.ItemCorreo);
        TextView Clave = (TextView) convertView.findViewById(R.id.ItemClave);
        TextView Estado= (TextView) convertView.findViewById(R.id.ItemEstado);

        Nombre.setText(Item.getNombre());
        Apellido.setText(Item.getApellido());
        Correo.setText(Item.getCorreo());
        Clave.setText(Item.getClave());
        String estado="DESHABILITADO";
        if (Item.getEstado()){
            estado="HABILITADO";
        }
       Estado.setText(estado);
        return convertView;
    }


    public static Boolean delete(ADM_AdaptadorUsuario Adaptador, int position) {
        try {
            ListaItem.remove(position);
            Adaptador= new ADM_AdaptadorUsuario(context, ListaItem, Lista_View);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }

    public static Boolean save(ADM_AdaptadorUsuario Adaptador, VENDEDOR VENDEDOR) {

        try {
            ListaItem.add(VENDEDOR);
            Adaptador = new ADM_AdaptadorUsuario(context, ListaItem, Lista_View);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }

    public static Boolean update(ADM_AdaptadorUsuario Adaptador, int position, VENDEDOR VENDEDOR) {

        try {

            ListaItem.remove(position);
            ListaItem.add(position, VENDEDOR);
            Adaptador= new ADM_AdaptadorUsuario(context, ListaItem, Lista_View);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }


}
