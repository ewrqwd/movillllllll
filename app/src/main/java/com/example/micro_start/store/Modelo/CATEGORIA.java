package com.example.micro_start.store.Modelo;

public class CATEGORIA {
    private  int Id;
    private  String Nombre;
    private  String Descripcion;
    private  Boolean Estado;

    public CATEGORIA() {
    }

    public CATEGORIA(int id, String nombre, String descripcion, Boolean estado) {
        Id = id;
        Nombre = nombre;
        Descripcion = descripcion;
        Estado = estado;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public Boolean getEstado() {
        return Estado;
    }

    public void setEstado(Boolean estado) {
        Estado = estado;
    }
}
