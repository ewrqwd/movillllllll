package com.example.micro_start.store.Controlador.Administrador;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.micro_start.store.Controlador.SharedPreferencesUsuario;
import com.example.micro_start.store.Controlador.UTILITARIOS;
import com.example.micro_start.store.Modelo.CATEGORIA;
import com.example.micro_start.store.Modelo.SISTEMA;
import com.example.micro_start.store.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ADM_FragmentCategoria extends Fragment {
    static SISTEMA SISTEMA = new SISTEMA();
    private UTILITARIOS Utilitarios = new UTILITARIOS();
    private ListView Lista_View;
    private ADM_AdaptadorCategoria Adaptador;
    private Dialog FormDialogo;
    private static final String URL = SISTEMA.getURL() + "/Admin/Categoria";
    ArrayList<CATEGORIA> LISTA = new ArrayList<>();
    //botones

    EditText InputNombre;
    EditText InputDescripcion;
    Switch SwitchEstado;

    Button Btn_agregar;
    Button Btn_editar;
    Button Btn_modificar;
    Button Btn_eliminar;
    Button Btn_cerrar;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_global, container, false);
        Lista_View = (ListView) view.findViewById(R.id.ListView);
        TextView View = (TextView) view.findViewById(R.id.form_titulo);
        View.setText("NOMINA DE CATEGORIA");
        Lista_View.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DisplayInputDialog(i, view);
            }
        });

        final FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisplayInputDialog(-1, view);
            }
        });
        BuscarRegistros(view);

        return view;
    }

    private void CargarLista(View view) {
        //cargar etiquetas
        Adaptador = new ADM_AdaptadorCategoria(view.getContext(), LISTA, Lista_View);
        Lista_View.setAdapter(Adaptador);

    }

    private void DisplayInputDialog(final int pos, final View view) {
        FormDialogo = new Dialog(view.getContext());
        FormDialogo.setTitle("NOMINA DE CATEGORIA");
        FormDialogo.setContentView(R.layout.dialogo_admin_categoria);
        //cargar etiquetas
        InputNombre = (EditText) FormDialogo.findViewById(R.id.InputNombre);
        InputDescripcion = (EditText) FormDialogo.findViewById(R.id.InputDescripcion);
        SwitchEstado = (Switch) FormDialogo.findViewById(R.id.SwitchEstado);

        Btn_agregar = (Button) FormDialogo.findViewById(R.id.Btn_agregar);
        Btn_editar = (Button) FormDialogo.findViewById(R.id.Btn_editar);
        Btn_modificar = (Button) FormDialogo.findViewById(R.id.Btn_modificar);
        Btn_eliminar = (Button) FormDialogo.findViewById(R.id.Btn_eliminar);
        Btn_cerrar = (Button) FormDialogo.findViewById(R.id.Btn_cerrar);

        Btn_modificar.setVisibility(View.INVISIBLE);
        switch (pos) {
            case -1:
                Btn_agregar.setVisibility(View.VISIBLE);
                Btn_editar.setVisibility(View.INVISIBLE);
                Btn_eliminar.setVisibility(View.INVISIBLE);
                HabilitarInput(true);

                break;
            default:
                Btn_agregar.setVisibility(View.INVISIBLE);
                Btn_editar.setVisibility(View.VISIBLE);
                Btn_eliminar.setVisibility(View.VISIBLE);
                //cargar elementos en etiquetas
                CATEGORIA Item = (CATEGORIA) Adaptador.getItem(pos);
                InputNombre.setText(Item.getNombre());
                InputDescripcion.setText(Item.getDescripcion());

                SwitchEstado.setChecked(Item.getEstado());
                break;
        }


        Btn_agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CATEGORIA obj = new  CATEGORIA();
                obj.setNombre(InputNombre.getText().toString());
                obj.setDescripcion(InputDescripcion.getText().toString());

                obj.setEstado(SwitchEstado.isChecked());
                String Validar = Utilitarios.ValidarFormularioCategoria(obj);
                if (Validar == "OK") {
                    AgregarRegistros(view, obj);
                } else {
                    Toast.makeText(view.getContext(), "INGRESE " + Validar, Toast.LENGTH_SHORT).show();
                }


            }
        });

        Btn_editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Btn_modificar.setVisibility(View.VISIBLE);
                Btn_editar.setVisibility(View.INVISIBLE);
                HabilitarInput(true);
            }
        });

        Btn_modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HabilitarInput(false);
                CATEGORIA ItemActual = (CATEGORIA) Adaptador.getItem(pos);
                CATEGORIA obj = new CATEGORIA();
                obj.setId(ItemActual.getId());
                obj.setNombre(InputNombre.getText().toString());
                obj.setDescripcion(InputDescripcion.getText().toString());

                obj.setEstado(SwitchEstado.isChecked());
                String Validar = Utilitarios.ValidarFormularioCategoria(obj);
                if (Validar == "OK") {
                    Btn_editar.setVisibility(View.VISIBLE);
                    Btn_modificar.setVisibility(View.INVISIBLE);
                    ModificarRegistros(view, obj, pos);
                } else {
                    HabilitarInput(true);
                    Toast.makeText(view.getContext(), "INGRESE " + Validar, Toast.LENGTH_SHORT).show();
                }
            }
        });

        Btn_eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               CATEGORIA ItemActual = (CATEGORIA) Adaptador.getItem(pos);
                EliminarRegistros(view, Adaptador, ItemActual.getId(), pos);

            }
        });

        Btn_cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FormDialogo.dismiss();
            }
        });
        FormDialogo.show();
    }

    private void Limpiar() {

        InputNombre.setText("");
        InputDescripcion.setText("");

        SwitchEstado.setChecked(true);
    }

    private void HabilitarInput(Boolean estado) {

        InputNombre.setEnabled(estado);
        InputDescripcion.setEnabled(estado);

        SwitchEstado.setEnabled(estado);
    }

    //MOSTRAR
    private void BuscarRegistros(final View view) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("BUSCANDO  ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    //  Integer id = (Integer) jsonObject.get("id");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject OB = jsonArray.getJSONObject(i);
                        int ID = (Integer) OB.get("id");
                        String NOMBRE = (String) OB.get("nombre");
                        String DESCRIPCION= (String) OB.get("descripcion");

                        int ESTADO_INT = (Integer) OB.get("estado");
                        Boolean ESTADO = false;
                        if (ESTADO_INT == 1) {
                            ESTADO = true;
                        }
                        LISTA.add(new CATEGORIA(ID,  NOMBRE, DESCRIPCION, ESTADO));
                    }
                    CargarLista(view);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "show");
                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }

    //AGREGAR
    private void AgregarRegistros(final View view, final CATEGORIA obj) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("CREANDO ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonArray = new JSONObject(response);
                    int ID = (int) jsonArray.get("id");
                    obj.setId(ID);
                    if (ADM_AdaptadorCategoria.save(Adaptador, obj)) {
                        Toast.makeText(context, "CATEGORIA GUARDADO", Toast.LENGTH_SHORT).show();
                        Limpiar();
                        FormDialogo.dismiss();
                    } else {
                        //mandar a refrescar si no se guardo el registro de manera local
                        BuscarRegistros(view);
                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "add");
                map.put("nombre", InputNombre.getText().toString());
                map.put("descripcion", InputDescripcion.getText().toString());

                String estado = "0";
                if (SwitchEstado.isChecked()) {
                    estado = "1";
                }
                map.put("estado", estado);

                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }

    //MODIFICAR
    private void ModificarRegistros(final View view, final CATEGORIA obj, final int POSCION) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("ACTUALIZANDO ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                    if (ADM_AdaptadorCategoria.update(Adaptador, POSCION, obj)) {
                        Toast.makeText(context, "DATOS MODIFICADOS", Toast.LENGTH_SHORT).show();
                        Btn_modificar.setVisibility(View.INVISIBLE);
                        Btn_editar.setVisibility(View.VISIBLE);
                    } else {
                        //mandar a refrescar si no se guardo el registro de manera local
                        BuscarRegistros(view);
                    }
                    progressDialog.dismiss();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "edi");
                int ID = obj.getId();
                map.put("id", String.valueOf(ID));
                map.put("nombre", InputNombre.getText().toString());
                map.put("descripcion", InputDescripcion.getText().toString());

                String estado = "0";
                if (SwitchEstado.isChecked()) {
                    estado = "1";
                }
                map.put("estado", estado);

                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }

    //MODIFICAR
    private void EliminarRegistros(final View view, final ADM_AdaptadorCategoria Adaptador, final int ID, final int POSCION) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("ELIMINADO..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (ADM_AdaptadorCategoria.delete(Adaptador, POSCION)) {
                    Toast.makeText(context, "CATEGORIA ELIMINADO", Toast.LENGTH_SHORT).show();
                    FormDialogo.dismiss();
                } else {
                    Toast.makeText(context, "NO SE ELIMINO LA CATEGORIA", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "del");
                map.put("id", String.valueOf(ID));
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }
}
