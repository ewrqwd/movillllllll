package com.example.micro_start.store.Controlador;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.micro_start.store.Modelo.*;

public class UTILITARIOS {
    //gestor de validaciones
    public String ValidarFormularioUsuario(VENDEDOR C) {
        String MSJ = "OK";
      if (C.getNombre().length() == 0) {
            MSJ = "Nombre";
        } else if (C.getApellido().length() == 0) {
            MSJ = "Apellido";
        } else if (C.getCorreo().length() == 0) {
            MSJ = "Correo";
        } else if (C.getClave().length() == 0) {
            MSJ = "Clave";
        }
        return MSJ;
    }
    //gestor de validaciones
    public String ValidarFormularioCategoria(CATEGORIA C) {
        String MSJ = "OK";
        if (C.getNombre().length() == 0) {
            MSJ = "Nombre";
        } else if (C.getDescripcion().length() == 0) {
            MSJ = "Apellido";
        }
        return MSJ;
    }
    //errrores
    public void MensajeError(Context context, ProgressDialog progressDialog, VolleyError error) {
        progressDialog.dismiss();
        Log.e("ERROR", error.toString());
        Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
    }
}
