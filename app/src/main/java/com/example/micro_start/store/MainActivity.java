package com.example.micro_start.store;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.micro_start.store.Controlador.*;
import com.example.micro_start.store.Controlador.Administrador.ADM_FragmentCategoria;
import com.example.micro_start.store.Controlador.Administrador.ADM_FragmentCliente;
import com.example.micro_start.store.Controlador.Administrador.ADM_FragmentVendedor;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //cargar cabecera

        View headerView = navigationView.getHeaderView(0);
        TextView rol = (TextView) headerView.findViewById(R.id.header_rol);
        String Rol = SharedPreferencesUsuario.BuscarDato("rol");
        rol.setText(Rol);
        TextView nombre = (TextView) headerView.findViewById(R.id.header_nombre);
        String MiNombre = SharedPreferencesUsuario.BuscarDato("nombre") + " " + SharedPreferencesUsuario.BuscarDato("apellido");
        nombre.setText(MiNombre);
        //mostar menus por rol
        int IDROL = Integer.parseInt(SharedPreferencesUsuario.BuscarDato("id_rol"));
        switch (IDROL) {
            case 1:
                navigationView.getMenu().findItem(R.id.g_admin).setVisible(true);
                navigationView.getMenu().findItem(R.id.g_vendedor).setVisible(false);
                navigationView.getMenu().findItem(R.id.g_clienet).setVisible(false);
                break;
            case 2:
                navigationView.getMenu().findItem(R.id.g_admin).setVisible(false);
                navigationView.getMenu().findItem(R.id.g_vendedor).setVisible(true);
                navigationView.getMenu().findItem(R.id.g_clienet).setVisible(false);
                break;
            case 3:
                navigationView.getMenu().findItem(R.id.g_admin).setVisible(false);
                navigationView.getMenu().findItem(R.id.g_vendedor).setVisible(false);
                navigationView.getMenu().findItem(R.id.g_clienet).setVisible(true);
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_salir) {
            finish();
            SharedPreferencesUsuario.getInstance(getApplicationContext()).CerrarCesssion();
            startActivity(new Intent(MainActivity.this, Login.class));
        }
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager Fragment = getSupportFragmentManager();
        switch (id) {
            case R.id.adm_vendedor:
                Fragment.beginTransaction().replace(R.id.contenedor, new ADM_FragmentVendedor()).commit();
                break;
           case R.id.adm_cliente:
              Fragment.beginTransaction().replace(R.id.contenedor, new ADM_FragmentCliente()).commit();
             break;
            case R.id.adm_categoria:
            Fragment.beginTransaction().replace(R.id.contenedor, new ADM_FragmentCategoria()).commit();
               break;
//            case R.id.adm_producto:
//                //    Fragment.beginTransaction().replace(R.id.contenedor, new FragmentPsicologoPaciente()).commit();
//                break;
//            case R.id.adm_venta:
//                //     Fragment.beginTransaction().replace(R.id.contenedor, new FragmentPsicologoTest()).commit();
//                break;
//            case R.id.ven_cliente:
//                //        Fragment.beginTransaction().replace(R.id.contenedor, new FragmentPacienteTest()).commit();
//                break;
//            case R.id.ven_producto:
//                //        Fragment.beginTransaction().replace(R.id.contenedor, new FragmentPacienteTest()).commit();
//                break;
//            case R.id.ven_venta:
//                //        Fragment.beginTransaction().replace(R.id.contenedor, new FragmentPacienteTest()).commit();
//                break;
//            case R.id.cli_compra:
//                //        Fragment.beginTransaction().replace(R.id.contenedor, new FragmentPacienteTest()).commit();
//                break;
//            case R.id.cli_producto:
//                //        Fragment.beginTransaction().replace(R.id.contenedor, new FragmentPacienteTest()).commit();
//                break;
            default:
                Fragment.beginTransaction().replace(R.id.contenedor, new FragmentPrincipal()).commit();
                break;
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
