package com.example.micro_start.store.Controlador.Administrador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.micro_start.store.Modelo.CATEGORIA;
import com.example.micro_start.store.R;

import java.util.ArrayList;

public class ADM_AdaptadorCategoria extends BaseAdapter {
    private static Context context;
    private static ArrayList<CATEGORIA> ListaItem;
    private static ListView Lista_View;

    public ADM_AdaptadorCategoria(Context context, ArrayList<CATEGORIA> listaItem, ListView lista_view) {
        this.context = context;
        this.ListaItem = listaItem;
        this.Lista_View = lista_view;
    }


    @Override
    public int getCount() {//cantidad de contenido
        return ListaItem.size();
    }

    @Override
    public Object getItem(int position) {//devuelve el list item de la posicion
        return ListaItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       CATEGORIA Item = (CATEGORIA ) getItem(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.item_categoria, null);
        TextView Nombre = (TextView) convertView.findViewById(R.id.ItemNombre);
        TextView Descripcion = (TextView) convertView.findViewById(R.id.ItemDescripcion);

        TextView Estado= (TextView) convertView.findViewById(R.id.ItemEstado);

        Nombre.setText(Item.getNombre());
        Descripcion.setText(Item.getDescripcion());

        String estado="DESHABILITADO";
        if (Item.getEstado()){
            estado="HABILITADO";
        }
    Estado.setText(estado);
        return convertView;
    }


    public static Boolean delete(ADM_AdaptadorCategoria Adaptador, int position) {
        try {
            ListaItem.remove(position);
            Adaptador= new ADM_AdaptadorCategoria(context, ListaItem, Lista_View);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }

    public static Boolean save(ADM_AdaptadorCategoria Adaptador,CATEGORIA OBJ) {

        try {
            ListaItem.add(OBJ);
            Adaptador = new ADM_AdaptadorCategoria(context, ListaItem, Lista_View);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }

    public static Boolean update(ADM_AdaptadorCategoria Adaptador, int position, CATEGORIA OBJ) {

        try {

            ListaItem.remove(position);
            ListaItem.add(position,OBJ);
            Adaptador= new ADM_AdaptadorCategoria(context, ListaItem, Lista_View);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }


}
